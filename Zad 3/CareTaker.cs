﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace Lv6Zad3
{
    class CareTaker
    {
        public List<Memento> PreviousStates;
        public CareTaker() {
            this.PreviousStates = new List<Memento>();
        }

        public void SaveState(Memento memento) {
            this.PreviousStates.Add(memento);
        }
        public Memento RestoreState() {
            if (this.PreviousStates == null)
            {
                return null;
            }
            else
            {
                
                return this.PreviousStates.Last();
            }
        }
        public Memento RestoreState(int n) {
            if (this.PreviousStates == null)
            {
                return null;
            }
            else
            {
                return this.PreviousStates[n];
            }
        }

        public void ClearAllSavedStates()
        {
            this.PreviousStates.Clear();

        }
        public void RemoveLastSavedState() {
            int lastelement=PreviousStates.Count;
            this.PreviousStates.RemoveAt(lastelement);
        }

    }
}
