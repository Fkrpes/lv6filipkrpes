﻿using System;

namespace Lv6Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box testBox = new Box();
            Product product1 = new Product("A priceless atztec stone mask", 1000000);
            Product product2 = new Product("An airsoft gas powerd VSS Vintorez", 200);
            Product product3 = new Product("A Compound bow", 400);

            testBox.AddProduct(product1);
            testBox.AddProduct(product2);
            testBox.AddProduct(product3);

            Iterator testIterator = (Iterator)testBox.GetIterator();
            for (Product currentProduct = testIterator.First(); testIterator.IsDone == false; currentProduct = testIterator.Next())
            {
                currentProduct.Show();

            }
        }
    }
}
