﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv6Zad2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
