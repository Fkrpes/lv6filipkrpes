﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv6Zad2
{
    interface IAbstractIterator
    {
        Product First();
        Product Next();
        bool IsDone { get; }
        Product Current { get; }

    }
}
