﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv6Zad1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
