﻿using System;

namespace Lv6Zad1
{
    class Program
    {
        static void Main(string[] args)
        {

            Notebook testNotebook = new Notebook();
            Note note1 = new Note("Reference1","The cake is a lie");
            Note note2 = new Note("Reference2", "Do you know the definition of insanity?");
            Note note3 = new Note("Reference3", "Its doing the same thing over and over and expect that something changes");
            testNotebook.AddNote(note1);
            testNotebook.AddNote(note2);
            testNotebook.AddNote(note3);
            Iterator testIterator = (Iterator)testNotebook.GetIterator();
            for (Note currentNote = testIterator.First(); testIterator.IsDone == false; currentNote=testIterator.Next()) {
                currentNote.Show();
            }

            



            
        }
    }
}
